package Ejercicio1;

public @interface Instrucciones {
	/*
	 * Ejercicio 1.
	 * 
	 * 
	 * Cread una clase no ejecutable llamada “Equipo”. Con las siguientes
	 * características: ○ Esta clase tendrá una variable de texto llamada “Nombre”.
	 * Solo se podrá acceder a esta variable directamente desde la propia clase. ○
	 * Existirá un método accesible para todas las clases que devolverá el valor de
	 * la variable nombre. ○ La clase tendrá un constructor en el que pasaremos el
	 * nombre con el que debe crearse el equipo. ○ No se podrá modificar
	 * externamente el valor de esa variable.
	 * 
	 * Ejercicio 2.
	 * 
	 * Cread una clase no ejecutable llamada “Jugador”. Con las siguientes
	 * características: ○ Esta clase tendrá variable de texto llamada “Nombre”. ○
	 * Tendrá una variable numérica llamada “Goles Anotados”. ○ Ambas variables solo
	 * podrán ser accedidas directamente por la propia clase. ○ Ambas variables
	 * tendrán un método para poder consultar sus valores. ○ Existirá una función
	 * que sume 1 al campo “Goles Anotados” cada vez que se le llame. ○ Existirá un
	 * constructor que recibirá el nombre a asignar al jugador y que automáticamente
	 * inicializará goles anotados a 0.
	 * 
	 * Ejercicio 3.
	 * 
	 * Añadid dos variables a la clase “Equipo” de tipo “Jugador”. Las llamaremos
	 * jugador1 y jugador2. ○ Cread dos funciones (una para cada jugador) para
	 * inicializar a los jugadores. Esta función recibirá como parámetro el nombre
	 * de cada jugador. ○ Cread una función que reciba un número indicando si se
	 * refiere al jugador 1 o al jugador 2. Esta función sumará un gol al jugador
	 * indicado
	 * 
	 * Ejercicio 4.
	 * 
	 * Cread una clase ejecutable partido. Esta clase realizará las siguientes
	 * acciones: ○ Declarará e inicializará dos variables equipos (equipo1 y
	 * equipo2) con los nombres que vosotros elijáis. ○ Para cada equipo se crearán
	 * los dos jugadores (jugador1 y jugador2) con los nombres que elijáis. ○ El
	 * jugador1 del equipo1 marcará un gol. ○ El jugador1 del equipo1 marcará un
	 * gol. ○ El jugador2 del equipo1 marcará un gol. ○ El jugador1 del equipo2
	 * marcará un gol. ○ El jugador2 del equipo1 marcará un gol.
	 * 
	 * 
	 * Ejercicio 5.
	 * 
	 *  Siguiendo las directrices vistas en clase, incluid el código
	 * necesario para que una vez acabado el ejercicio 15 se muestre lo siguiente: ○
	 * Goles totales del equipo 1 ○ Goles totales del equipo 2 ○ Goles del jugador1
	 * del equipo1 ○ Goles del jugador 2 del equipo1 ○ Goles del jugador1 del
	 * equipo2 ○ Goles del jugador 2 del equipo2
	 * 
	 */

}
