package Ejercicio1;

public class Partido {
public static void main(String[] args) {
	Equipo equipo1 = new Equipo("Barcelona");
	Equipo equipo2 = new Equipo("Madrid");
	
	equipo1.iniciarJugador1("Rafa");
	equipo1.iniciarJugador2("Alberto");
	equipo2.iniciarJugador1("Juan");
	equipo2.iniciarJugador2("Mario");
	
	equipo1.sumarGol(1);
	equipo1.sumarGol(1);
	equipo1.sumarGol(2);
	equipo2.sumarGol(1);
	equipo1.sumarGol(2);
	
	System.out.println("Goles totales equipo 1: " + equipo1.getGolesAnotadosTotales());
	System.out.println("Goles totales equipo 2: " + equipo2.getGolesAnotadosTotales());
	System.out.println("Goles jugador 1 equipo 1: " + equipo1.getGolesAnotadosPorJugador(1));
	System.out.println("Goles jugador 2 equipo 1: " + equipo1.getGolesAnotadosPorJugador(2));
	System.out.println("Goles jugador 1 equipo 2: " + equipo2.getGolesAnotadosPorJugador(1));
	System.out.println("Goles jugador 2 equipo 2: " + equipo2.getGolesAnotadosPorJugador(2));
}
}
