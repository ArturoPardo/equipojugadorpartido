package Ejercicio1;

public class Jugador {
	private String nombre;
	private int golesAnotados;
	
	public Jugador(String _nombre) {
		this.nombre = _nombre;
		this.golesAnotados = 0;
		
	}

	public void anotarGol() {
		this.golesAnotados++;
	}

	public String getNombre() {
		return nombre;
	}

	public int getGolesAnotados() {
		return golesAnotados;
	}

}
