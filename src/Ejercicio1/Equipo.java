package Ejercicio1;

public class Equipo {
	private String nombre;
	private Jugador jugador1;
	private Jugador jugador2;

	public Equipo(String _nombre) {
		this.nombre = _nombre;
	}

	public void iniciarJugador1(String _nombre) {

		this.jugador1 = new Jugador(_nombre);
	}

	public void iniciarJugador2(String _nombre) {

		this.jugador2 = new Jugador(_nombre);
	}
	

	public void sumarGol(int _jugador) {
		if (_jugador == 1) {
			this.jugador1.anotarGol();
		} else if (_jugador == 2) {
			this.jugador2.anotarGol();

		} else {
			System.out.println("el jugador no existe");
		}

	}
	
	public int getGolesAnotadosPorJugador(int jugador) {
		int golesJugador = 0;
		if (jugador == 1) {
			golesJugador = this.jugador1.getGolesAnotados();
		} else if (jugador == 2) {
			golesJugador = this.jugador2.getGolesAnotados();
		} else {
			System.out.println("El jugador introducido no existe,");
		}
		return golesJugador;
	}
	
	public int getGolesAnotadosTotales() {
		return this.jugador1.getGolesAnotados() + this.jugador2.getGolesAnotados();
	}

	public String getNombre() {
		return nombre;
	}

}
